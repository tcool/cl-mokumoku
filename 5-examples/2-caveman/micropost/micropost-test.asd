(defsystem "micropost-test"
  :defsystem-depends-on ("prove-asdf")
  :author ""
  :license ""
  :depends-on ("micropost"
               "prove")
  :components ((:module "tests"
                :components
                ((:test-file "micropost"))))
  :description "Test system for micropost"
  :perform (test-op (op c) (symbol-call :prove-asdf :run-test-system c)))
