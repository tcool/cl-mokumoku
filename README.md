![screencast](screenshot/screen.png)

# 関西もくもく.lisp

## 日 時 ・ 場 所

[connpass](https://tcool.connpass.com)でご確認ください。

## 内　容

Common LispでWebアプリケーションを開発するもくもく会です。

## 進 め 方

Slackで情報交換をしながら、各自で開発を進めます。

## 教　材

- [Full Stack Lisp](https://leanpub.com/fullstacklisp/read)

## スケジュール

| 時間 | タイトル |
|:----:|:------:|
| 13:00 - 13:15 | 開場、受付 |
| 13:15 - 17:00 | もくもく会 |
| 17:00 - 20:00 | 懇親会 |

## 持 ち 物

ノートパソコン

Wikiページを参考に、環境構築をしてきてください。

## 参 加 費

500円

## 運  営

英語・パソコン教室 ツクール・関西Lispユーザ会 共催